package mavenproject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.StringTokenizer;

public class SearchTshirt {

	ArrayList<BinClass> tShirtList = new ArrayList<BinClass>();

	// arr arraylist obj create for storing file token obj(string)
	ArrayList<String> arr;

	// search user input record into the local csv files
	public void searchData(String filename, String colour, String size, String gender) throws FileNotFoundException {
		Scanner sc = new Scanner(new File(filename));
		while (sc.hasNext()) {
			String line = sc.nextLine().toUpperCase().toString();
			if (!line.isEmpty()) {
				// for spilit the string (line string) using delimeter |
				StringTokenizer token = new StringTokenizer(line, "|");
				arr = new ArrayList<String>(line.length());
				// for store the string elements into the list object
				while (token.hasMoreTokens()) {
					arr.add(token.nextToken());
				}

				if (arr.get(2).equalsIgnoreCase(colour) && arr.get(3).equalsIgnoreCase(size)
						&& arr.get(4).equalsIgnoreCase(gender)) {
					BinClass bc = new BinClass(arr.get(0), arr.get(1), arr.get(2), arr.get(3), arr.get(4),
							Float.parseFloat(arr.get(5)), Float.parseFloat(arr.get(6)), arr.get(7));
					tShirtList.add(bc);
				}
			}
		}
	}

	public ArrayList<BinClass> record() {
		return tShirtList;
	}

}