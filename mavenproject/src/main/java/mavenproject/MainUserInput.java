package mavenproject;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

//user define exception
class MyException extends Exception {
}

public class MainUserInput {

	public static void main(String[] args) throws FileNotFoundException {
		Scanner sc = new Scanner(System.in);
//       take required input from the user
		boolean yes = true;
		while (yes) {
			System.out.println("Enter T-Shirt Colour");
			String colour = sc.nextLine().toUpperCase();

			boolean a = true;
			String size = "";
			while (a) {
				System.out.println("Enter T-Shirt Size");
				size = sc.nextLine().toUpperCase();
				if (size.equals("S") || size.equals("M") || size.equals("L") || size.equals("XL")
						|| size.equals("XXL")) {
					a = false;
				} else {
					try {
						throw new MyException();
					} catch (MyException ex) {
						System.out.println("Wrong input please select  S, M , L , XL, XXL");
					}
				}
			}
			boolean b = true;
			String gender = "";
			while (b) {
				System.out.println("Enter  Gender");
				System.out.println("Select M or F or U");
				gender = sc.nextLine().toUpperCase();
				if (gender.equals("M") || gender.equals("F") || gender.equals("U")) {
					b = false;
				} else {
					try {
						throw new MyException();
					} catch (MyException ex) {
						System.out.println("Wrong input please select M for male , F for female, U for uni...");
					}
				}
			}
			System.out.println("Please->  Select Output Prefrence");
			System.out.println("press 1:  sorted only by Price ");
			System.out.println("press 2:  sorted only by Rating");
			System.out.println("press 3:  sorted by both Price and Rating");
			int choice = sc.nextInt();

			// for searching user input record into the files
			SearchTshirt st = new SearchTshirt();
			String file1 = "C:\\Users\\ashishjaiswal\\Desktop\\Assigment Links\\Adidas.csv";
			st.searchData(file1, colour, size, gender);
			String file2 = "C:\\Users\\ashishjaiswal\\Desktop\\Assigment Links\\Nike.csv";
			st.searchData(file2, colour, size, gender);
			String file3 = "C:\\Users\\ashishjaiswal\\Desktop\\Assigment Links\\Puma.csv";
			st.searchData(file3, colour, size, gender);
			String file4 = "C:\\Users\\ashishjaiswal\\AppData\\Local\\Temp\\Temp1_Assigment Links.zip\\Assigment Links\\Custmize.csv";
			st.searchData(file4, colour, size, gender);
			System.out.println("mission succesfull");

			OutputPrefrence ot = new OutputPrefrence();
			// tshirtlist arraylist obj create for storing matching data
			ArrayList<BinClass> tShirtList = new ArrayList<BinClass>();
			tShirtList = st.record();

			// for getting result according to selected user output prefrence
			ot.outputPrefrence(choice, tShirtList);
			System.out.println("you want to search more records press yes or no");
			sc.nextLine();
			String n = sc.nextLine();
			if (n.equalsIgnoreCase("yes"))
				yes = true;
			else
				yes = false;

		}

	}
}
