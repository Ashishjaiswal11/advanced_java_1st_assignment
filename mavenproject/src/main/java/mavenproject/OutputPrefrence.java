package mavenproject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class OutputPrefrence {
	DataView dv = new DataView();

	public void outputPrefrence(int choice, ArrayList<BinClass> tShirtList) {

		// checking user output prefrence and sort
		if (choice == 1) {
			// using lamba function (java 8 feature)
			Comparator<BinClass> c1 = (BinClass o1, BinClass o2) -> (int) (o1.getPrice() - o2.getPrice());
			Collections.sort(tShirtList, c1);
		}

		// checking user output prefrence and sort
		else if (choice == 2) {
			Comparator<BinClass> c1 = (BinClass o1, BinClass o2) -> (int) (o2.getRating() - o1.getRating());
			Collections.sort(tShirtList, c1);
		}

		// checking user output prefrence and sort
		else {
			Comparator<BinClass> c1 = (BinClass o1, BinClass o2) -> (int) (o1.getRating() - o2.getRating())
					- (int) (o1.getPrice() - o2.getPrice());
			Collections.sort(tShirtList, c1);
		}

		// show matching record
		dv.viewTshirts(tShirtList);

	}
}