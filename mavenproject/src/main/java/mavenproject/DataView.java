package mavenproject;

import java.util.ArrayList;

class NotAvailable extends Exception {

}

public class DataView {
	// for display list of t-shirt with details
	public void viewTshirts(ArrayList<BinClass> tShirtList) {
		System.out.println("\n \t\t\t\t\t*****T-Shirt INFORMATION *****");
		System.out.println();
		System.out.println(
				"    ID     |            NAME            |COLOUR|GENDER_RECOMMENDATION|SIZE|PRICE|RATING|AVAILABILITY");
		System.out.println();
		int flag = 0;
		for (BinClass f : tShirtList) {
			System.out.println(f.getId() + " | " + f.getName() + " | " + f.getColour() + " |          " + f.getGender()
					+ "          | " + f.getSize() + " | " + f.getPrice() + " | " + f.getRating() + " |    "
					+ f.getAvailability() + "    | ");
			flag++;
			System.out.println();
		}
		if (tShirtList.isEmpty()) {
			try {
				throw new NotAvailable();
			} catch (NotAvailable ex) {
				System.out.println();
				System.out.println("no record found");
			}
		} else {
			try {
				throw new NotAvailable();
			} catch (NotAvailable ex) {
				System.out.println();
				System.out.println(" " + flag + " record matching ");
			}
		}
	}

}
